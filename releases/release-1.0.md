---
description: Release 1.0 for Android 14
---

# Release 1.0

This release was built on an Android 14 AOSP. May work on other Android versions.

SURFACE\_NUM is equal to 32 in this release.

Link to release on gitlab: [https://gitlab.com/all\_projects885526/android\_projects/hw\_layers/-/releases/release\_1.0](https://gitlab.com/all\_projects885526/android\_projects/hw\_layers/-/releases/release\_1.0)
