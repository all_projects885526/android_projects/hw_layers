---
description: >-
  App that generates many layers to get the number of hardware layers that
  Android has available.
---

# Introduction

## Hw\_Layers

### Name

HW Layers

### Description

This app generate many Layers. Android always will try to manage the maximum possible number of layers by hardware layers. This way you can get the number of hardware layers available on a device.

### Build

1. Open terminal, henceforth T1
2. On T1, cd to root of AOSP
3. On T1, run `. ./build/envsetup.sh`
4. On T1, run lunch command:
   * For BeaglePlay run: `lunch am62x_car-userdebug`
5. On T1, cd to this project (that should be locate in any sub-directory of AOSP)
6. On T1, run `mm`
7. Generated APK should be located on "\<AOSP\_path>/out/target/product//system/app/hwlayer\_test/hwlayer\_test.apk":
   * On BeaglePlay should be located on "\<AOSP\_path>/out/target/product/am62x/system/app/hwlayer\_test/hwlayer\_test.apk"

### Installation

* `adb install <AOSP_path>/out/target/product/am62x/system/app/hwlayer_test/hwlayer_test.apk`

### Usage

SURFACE\_NUM is equal to 32 in this project. If you want to keep this number, you can download a [release](broken-reference) and follow from step 4.

1. Go to app/src/main/java/net/danlew/hwlayers/MainActivity.java
2. Set the desired number of layers: MainActivity:SURFACE\_NUM (This number should be grater than suspected numbers of hardware layers)
3. [Build](introduction.md#build)
4. [Install APK](introduction.md#installation)
5. Run APK
6. Run the following ADB command: `adb shell dumpsys SurfaceFlinger | sed -n '/HWC layers/,/Planner/p'`
7. Check number of available hardware layers on your device:
   * if "Comp Type" is "DEVICE", it's a hardware layer
   * if "Comp Type" is "CLIENT", it's a software layer
8. Note: Always there is 1 hardware layer plus the number of "DEVICE" layers. This is because all other "CLIENT" layers are composed in one extra hardware layer.
