package com.mirgor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.util.Log;
import android.graphics.Rect;

public class CustomSurfaceView extends SurfaceView {
    private String LOG_TAG = "HwLayer";
    private SurfaceHolder surfaceHolder;
    private Bitmap bmpIcon;

    public CustomSurfaceView(Context context) {
        super(context);
        init();
    }

    public CustomSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    
    private void init() {
        surfaceHolder = getHolder();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        bmpIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher, options);
        bmpIcon.setHasAlpha(true);
        surfaceHolder.addCallback(new SurfaceHolder.Callback(){
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Log.d(LOG_TAG, "surfaceCreated");
                Canvas canvas = holder.lockHardwareCanvas();
                drawSomething(canvas);
                holder.unlockCanvasAndPost(canvas);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, 
                int format, int width, int height) {
                // TODO Auto-generated method stub
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                // TODO Auto-generated method stub
            }   
        });
    }

    protected void drawSomething(Canvas canvas) {
        Log.d(LOG_TAG, "canvas.isHardwareAccelerated?: " + canvas.isHardwareAccelerated());
        canvas.drawColor(Color.TRANSPARENT);
        Rect src = new Rect(0, 0, bmpIcon.getWidth(), bmpIcon.getHeight());
        Rect dst = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());
        canvas.drawBitmap(bmpIcon, src, dst, null);
    }
}
