package com.mirgor;

import static android.view.WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE;

import android.app.Activity;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowInsets;
import android.view.WindowInsetsController;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.util.DisplayMetrics;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import android.graphics.Color;
import android.graphics.PixelFormat;

public class MainActivity extends Activity {

    static final private String LOG_TAG = "HwLayer";
    static final private int SURFACE_NUM = 32;

    private LinearLayout mParentLayout;
    private RelativeLayout mParentSurfaceLayout;
    private List<CustomSurfaceView> mSurfaceView = new ArrayList<CustomSurfaceView>();
    private CheckBox mHwLayerCheckbox;
    private int mWindowWidth;
    private int mWindowHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hideSysUI();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        mWindowWidth = displayMetrics.widthPixels;
        mWindowHeight = displayMetrics.heightPixels;

        setContentView(R.layout.activity_main);
        mParentLayout = findViewById(R.id.parent_layout);

        Log.d(LOG_TAG, "view.isHardwareAccelerated?: " + mParentLayout.isHardwareAccelerated());

        mHwLayerCheckbox = (CheckBox) findViewById(R.id.hw_layers_checkbox);

        mHwLayerCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                changeLayerType();
            }
        });

        mParentSurfaceLayout = (RelativeLayout) findViewById(R.id.parent_surface_layout);
        float fheight = mWindowHeight;
        fheight = fheight * 0.9f;
        fheight = fheight / SURFACE_NUM;
        int height = (int)fheight;
        CustomSurfaceView prevSurface = null;
        for(int i=0; i<SURFACE_NUM; i++) {
            CustomSurfaceView surface = new CustomSurfaceView(this);
            SurfaceHolder holder = surface.getHolder();
            holder.setFixedSize(height, height);
            surface.setId(View.generateViewId());
            surface.setBackgroundColor(Color.TRANSPARENT);
            surface.setZOrderOnTop(true);    // necessary
            holder.setFormat(PixelFormat.TRANSPARENT);
            if(prevSurface != null) {
                RelativeLayout.LayoutParams surfaceParam = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
                surfaceParam.addRule(RelativeLayout.BELOW, mParentSurfaceLayout.getChildAt(i-1).getId());
                surface.setLayoutParams(surfaceParam);
            }
            mSurfaceView.add(surface);
            mParentSurfaceLayout.addView(surface);
            prevSurface = surface;
        }

        changeLayerType();

        int i=1;
        for(CustomSurfaceView surface : mSurfaceView) {
            animate(surface, i*1000);
            i++;
        }
    }

    private void hideSysUI() {
        View decorView = getWindow().getDecorView();
        WindowInsetsController windowInsetsController = decorView.getWindowInsetsController();
        windowInsetsController.hide(WindowInsets.Type.systemBars());
        windowInsetsController.setSystemBarsBehavior(BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
    }

    private void changeLayerType() {
        if (mHwLayerCheckbox.isChecked()) {
            for(CustomSurfaceView surface : mSurfaceView) {
                surface.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }
        } else {
            for(CustomSurfaceView surface : mSurfaceView) {
                surface.setLayerType(View.LAYER_TYPE_NONE, null);
            }
        }
    }

    private void animate(CustomSurfaceView surface, int duration) {
        ObjectAnimator translation = ObjectAnimator.ofFloat(surface, "translationX", 0, mWindowWidth - 100f);
        translation.setDuration(duration);
        translation.setRepeatCount(ValueAnimator.INFINITE);
        translation.start();
    }
}
